pipeline {
   agent any
   stages {
      stage('UpdateGit'){
	steps{ 
		powershell '''
			cd "C:/Users/cristobal.silva/REPO/jenkins/jenkins"
      			get-date -format 'dd-MM-yyyy' >> "Historial/respaldo$(get-date -format 'dd-MM-yyyy').txt"
			git add .
			git commit -m "respaldo $(get-date -format 'dd-MM-yyyy')"
			git status >> "respaldo$(get-date -format 'dd-MM-yyyy').txt"
			git push
		'''
	}
      }
      stage('Respaldo') {
         steps {
             powershell '''
              Compress-Archive "C:/Users/cristobal.silva/REPO/jenkins/jenkins" "C:/Users/cristobal.silva/RESPALDOS/respaldo $(get-date -format 'dd-MM-yyyy')"
             '''
         }
      }
   }
}